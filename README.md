# Evolutionary learning of interpretable decision trees 
This repo hosts the code of the paper [Multi-objective automatic analysis of lung ultrasound data from
COVID-19 patients by means of deep learning and decision trees].

``@misc{custode2022multiobjective,
      title={Multi-objective automatic analysis of lung ultrasound data from
COVID-19 patients by means of deep learning and decision trees},
      author={Leonardo Lucio Custode, Federico Mento, Francesco Tursi, Andrea Smargiassi, Riccardo Inchingolo, Tiziano Perrone, Libertario Demi, Giovanni Iacca},
      year={2022},
}``

## Installation
This implementation uses the [DEAP](https://github.com/DEAP/deap) library.
To install all the requirements use `pip install -r requirements.txt`

## Summary of the files
### prepare_*.py
This set of files allow for the preparation of the training data.

### evolve_dt_lus.py
This script is the main script, and allows to evolve decision trees on the multiobjective setup.
