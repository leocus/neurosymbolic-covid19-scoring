#!/usr/bin/env python
import os
import numpy as np
import pandas as pd
from tqdm import tqdm


def cat(*args):
    return os.path.join("Dataset", *args)


def ld(*args):
    return os.listdir(cat(*args))


train = ["Pavia COVID", "Lodi COVID", "Roma COVID"]
test = ["Lodi COVID", "Roma COVID"]
lab_suffix = "_crop_video_{}_{}_score.csv"
seg_suffix = "_segmented_video_{}_{}_segmentation.csv"


def create_dataset(train, X_name, y_name):
    X = []
    y = []

    for t in train:
        elements = ld(t)

        gt_file = None
        gt_df = None
        for e in elements:
            if "csv" in e:
                gt_file = e
                fname = cat(t, gt_file)
                gt_df = pd.read_csv(fname, delimiter=";", skiprows=[1])
                break

        pbar = tqdm(elements)
        for e in pbar:
            if os.path.isdir(cat(t, e)):
                files = ld(t, e)
                prefix = cat(t, e, "_".join(files[0].split("_")[:3]))
                suffix_number = files[0].split("_")[5]
                for i in range(1, 15):
                    try:
                        pbar.set_postfix_str(e + "_" + str(i))
                        gt = gt_df[gt_df["#"] == e][f"zone {i}"].values[0]
                        if gt == "x":
                            continue
                        else:
                            gt = int(gt)

                        cur_sample = []

                        fname = prefix + lab_suffix.format(suffix_number, i)
                        df = pd.read_csv(fname, delimiter=";", skiprows=[0])
                        scores = df["SCORE ASSEGNATO"].values

                        # Add the predictions of the labeling network
                        cur_sample.extend([
                            np.sum(scores == j)/len(scores) for j in range(4)
                        ])
                        cur_sample.append(np.min(scores))
                        cur_sample.append(np.max(scores))

                        fname = prefix + seg_suffix.format(suffix_number, i)
                        df = pd.read_csv(fname, delimiter=";", skiprows=[0])
                        scores = df["SCORE ASSEGNATO"].values
                        seg_scores = []

                        # Add the predictions of the segmentation network
                        for row, s in enumerate(scores):
                            for j in range(4, -1, -1):
                                if f"{j}" in s:
                                    seg_scores.append(j - 1)
                                    # break
                        seg_scores = np.array(seg_scores)

                        cur_sample.extend([
                            np.sum(
                                np.sum(seg_scores == j)
                            )/len(scores) for j in range(4)
                            # np.sum(
                            #     [
                            #         f"{j}" in s for s in scores
                            #     ])/len(scores) for j in range(1, 5)
                        ])
                        cur_sample.append(np.min(seg_scores))
                        cur_sample.append(np.max(seg_scores))

                        X.append(cur_sample)
                        y.append(gt)
                    except Exception as exc:
                        print(f"Error while retrieving data for {t}/{e}/{i}")
                        print(exc)
                        print("SCORES")
                        print(scores)
                        print("SEGSCORES")
                        print(seg_scores)

    X = np.array(X)
    X[np.isnan(X)] = 0
    y = np.array(y)
    np.save(X_name, X)
    np.save(y_name, y)


create_dataset(train, "XC.npy", "yC.npy")
create_dataset(test, "TXC.npy", "TyC.npy")
