#!/usr/bin/env bash
rm *.pkl
rm logs/ -rf

echo "============================== EXAM =============================="
# rm -rf Prognostic
# python3 prepare_prognostic_test_data.py -w
# mv Prognostic/X4.pkl ../Xtest.pkl
# mv Prognostic/y4.pkl ../ytest.pkl
for tol in 2 5 10; do
for i in {0..9}; do python3 make_dataset.py Prognostic/; mkdir -p logs/prognostic_$tol/Folds/$i; cp *.pkl logs/prognostic_$tol/Folds/$i/; python3 lus_exam_test_orthogonal.py --lambda_ 1000 --jobs 28 --genotype_len 50 --episodes 1 --mp 1 --cxp 0.8 --generations 1000 --selection function-tools.selBest --mutation function-tools.mutUniformInt\#low-0\#up-40000\#indpb-0.05 --seed $i --input_space 12 --tol $tol; done
mv logs/gym/* logs/prognostic_$tol;
done

# echo "============================== AREA =============================="
# python3 prepare_folds_area.py -w
# echo "------------------------- Max tolerance 0 ------------------------"
# for i in {0..9}; do python3 make_dataset.py Folds/Area/; mkdir -p logs/area_mt0/Folds/$i; cp *.npy logs/area_mt0/Folds/$i/; python3 lus_test_orthogonal.py --lambda_ 50 --jobs 28 --genotype_len 200 --episodes 1 --mp 1 --cxp 0.1 --generations 10000 --selection function-tools.selBest --mutation function-tools.mutUniformInt\#low-0\#up-40000\#indpb-0.05 --seed $i --input_space 12 --max_disagreement 0; done
# mv logs/gym/* logs/area_mt0
# echo "------------------------- Max tolerance 1 ------------------------"
# for i in {0..9}; do python3 make_dataset.py Folds/Area/; mkdir -p logs/area_mt1/Folds/$i; cp *.npy logs/area_mt1/Folds/$i/; python3 lus_test_orthogonal.py --lambda_ 50 --jobs 28 --genotype_len 200 --episodes 1 --mp 1 --cxp 0.1 --generations 10000 --selection function-tools.selBest --mutation function-tools.mutUniformInt\#low-0\#up-40000\#indpb-0.05 --seed $i --input_space 12 --max_disagreement 1; done
# mv logs/gym/* logs/area_mt1
# 
# echo "============================== EXAM =============================="
# python3 prepare_folds_exam.py -w
# echo "------------------------- Max tolerance 0 ------------------------"
# for i in {0..9}; do python3 make_dataset.py Folds/Exam/; mkdir -p logs/exam_mt0/Folds/$i; cp *.npy logs/exam_mt0/Folds/$i/; python3 lus_test_orthogonal.py --lambda_ 50 --jobs 28 --genotype_len 200 --episodes 1 --mp 1 --cxp 0.1 --generations 10000 --selection function-tools.selBest --mutation function-tools.mutUniformInt\#low-0\#up-40000\#indpb-0.05 --seed $i --input_space 12 --max_disagreement 0; done
# mv logs/gym/* logs/exam_mt0
# echo "------------------------- Max tolerance 1 ------------------------"
# for i in {0..9}; do python3 make_dataset.py Folds/Exam/; mkdir -p logs/exam_mt1/Folds/$i; cp *.npy logs/exam_mt1/Folds/$i/; python3 lus_test_orthogonal.py --lambda_ 50 --jobs 28 --genotype_len 200 --episodes 1 --mp 1 --cxp 0.1 --generations 10000 --selection function-tools.selBest --mutation function-tools.mutUniformInt\#low-0\#up-40000\#indpb-0.05 --seed $i --input_space 12 --max_disagreement 1; done
# mv logs/gym/* logs/exam_mt1
# 
# echo "============================== PATIENT =============================="
# python3 prepare_folds_patient.py -w
# echo "------------------------- Max tolerance 0 ------------------------"
# for i in {0..9}; do python3 make_dataset.py Folds/Patient/; mkdir -p logs/patient_mt0/Folds/$i; cp *.npy logs/patient_mt0/Folds/$i/; python3 lus_test_orthogonal.py --lambda_ 50 --jobs 28 --genotype_len 200 --episodes 1 --mp 1 --cxp 0.1 --generations 10000 --selection function-tools.selBest --mutation function-tools.mutUniformInt\#low-0\#up-40000\#indpb-0.05 --seed $i --input_space 12 --max_disagreement 0; done
# mv logs/gym/* logs/patient_mt0
# echo "------------------------- Max tolerance 1 ------------------------"
# for i in {0..9}; do python3 make_dataset.py Folds/Patient/; mkdir -p logs/patient_mt1/Folds/$i; cp *.npy logs/patient_mt1/Folds/$i/; python3 lus_test_orthogonal.py --lambda_ 50 --jobs 28 --genotype_len 200 --episodes 1 --mp 1 --cxp 0.1 --generations 10000 --selection function-tools.selBest --mutation function-tools.mutUniformInt\#low-0\#up-40000\#indpb-0.05 --seed $i --input_space 12 --max_disagreement 1; done
# mv logs/gym/* logs/patient_mt1
# 
