import os
import sys
import pickle
import numpy as np


ddir = sys.argv[1]
n_folds = len(list(filter(lambda x: "X" in x, os.listdir(ddir))))


validation_fold = np.random.randint(0, n_folds)


Xt = []
yt = []
Xv = []
yv = []

for i in range(n_folds):
    xfile = f"{ddir}/X{i}.pkl"
    yfile = f"{ddir}/y{i}.pkl"

    if i == validation_fold:
        Xv = pickle.load(open(xfile, "rb"))
        yv = pickle.load(open(yfile, "rb"))
    else:
        Xt.extend(pickle.load(open(xfile, "rb")))
        yt.extend(pickle.load(open(yfile, "rb")))


pickle.dump(Xt, open("X.pkl", "wb"))
pickle.dump(yt, open("y.pkl", "wb"))
pickle.dump(Xv, open("Xv.pkl", "wb"))
pickle.dump(yv, open("yv.pkl", "wb"))
