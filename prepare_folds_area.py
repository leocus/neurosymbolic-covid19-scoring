#!/usr/bin/env python
import os
import sys
import numpy as np
import pandas as pd
from tqdm import tqdm


n_folds = 14
base = "Dataset"
directories = ["Pavia COVID", "Lodi COVID", "Roma COVID"]
lab_struct = "{}_{}_{}_crop_video_{}_{}_score.csv"
seg_struct = "{}_{}_{}_segmented_video_{}_{}_segmentation.csv"


def compute_features_labeling(data):
    scores = data["SCORE ASSEGNATO"].values

    # Add the predictions of the labeling network
    cur_sample = [
        np.sum(scores == j)/len(scores) for j in range(4)
    ]
    cur_sample.append(np.min(scores))
    cur_sample.append(np.max(scores))

    return cur_sample


def compute_features_segmentation(data, only_worst=False):
    scores = data["SCORE ASSEGNATO"].values
    seg_scores = []

    # Add the predictions of the segmentation network
    for row, s in enumerate(scores):
        for j in range(4, -1, -1):
            if f"{j}" in s:
                seg_scores.append(j - 1)
                if only_worst:
                    break
    seg_scores = np.array(seg_scores)
    cur_sample = [
        np.sum(
            np.sum(seg_scores == j)
        )/len(scores) for j in range(4)
    ]
    cur_sample.append(np.min(seg_scores))
    cur_sample.append(np.max(seg_scores))

    return cur_sample


def compute_exam_features(d, only_worst=False):
    """
    Computes the features for all the samples contained in the directory

    :d: the directory
    :returns: A list of tuples (x, y)
    """
    dirname = "/".join(d.split("/")[:-1])
    f = os.path.join(d, "../", list(filter(lambda x: "csv" in x, os.listdir(dirname)))[0])
    exam_name = d.split("/")[-1]
    a, patient, n = os.listdir(d)[0].split("_")[:3]

    gtfile = pd.read_csv(f, delimiter=";", skiprows=[1])

    X = []
    y = []

    for i in range(1, 15):
        gt = gtfile[gtfile["#"] == exam_name][f"zone {i}"].values[0]

        if gt == "x":
            X.append(None)
            y.append(None)
            continue
        else:
            gt = int(gt)

        tmp_fname = os.path.join(d, lab_struct.format(a, patient, n, n, i))
        try:
            data = pd.read_csv(tmp_fname, skiprows=[0], delimiter=";")
        except:
            X.append(None)
            y.append(None)
            continue

        cur_sample = compute_features_labeling(data)

        tmp_fname = os.path.join(d, seg_struct.format(a, patient, n, n, i))
        try:
            data = pd.read_csv(tmp_fname, skiprows=[0], delimiter=";")
        except:
            X.append(None)
            y.append(None)
            continue

        cur_sample.extend(compute_features_segmentation(data, only_worst))

        X.append(np.array(cur_sample))
        y.append(gt)
    X = np.array(X)
    y = np.array(y)
    return X, y


exams = []

for d in directories:
    files = list(map(lambda x: os.path.join(base, d, x), os.listdir(os.path.join(base, d))))
    for f in files:
        if os.path.isdir(f):
            patient_id = int(f.split("/")[-1].split("_")[0])
            exams.append((patient_id, f))

only_worst = sys.argv[1] == "-w"

X, y = [], []
for _, exam in exams:
    samples, gt = compute_exam_features(exam, only_worst)
    X.extend(samples)
    y.extend(gt)

exams = [(X[i], y[i]) for i in range(len(X))]
folds = [[] for _ in range(n_folds)]
for i, e in enumerate(exams):
    if e[0] is not None:
        folds[i % n_folds].append(e)

for i in range(len(folds)):
    X, y = [], []
    for x, gt in folds[i]:
        X.append(np.array(x))
        y.append(np.array(gt))
    folds[i] = (np.array(X), np.array(y))

for i, (X, y) in enumerate(folds):
    os.makedirs("Folds/Area", exist_ok=True)
    np.save(os.path.join("Folds", "Area", f"X{i}.npy"), X)
    np.save(os.path.join("Folds", "Area", f"y{i}.npy"), y)

sum_ = 0
for f in folds:
    sum_ += len(f[0])
    print(len(f[0]))
print("total:", sum_)
