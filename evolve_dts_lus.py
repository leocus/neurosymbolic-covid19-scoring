import os
import gym
import json
import pickle
import string
import datetime
import argparse
import subprocess
import numpy as np
from time import time, sleep
from numpy import random
from dt import EpsGreedyLeaf, PythonDT, RandomlyInitializedEpsGreedyLeaf
from grammatical_evolution import GrammaticalEvolutionTranslator, grammatical_evolution, mo_grammatical_evolution


def string_to_dict(x):
    """
    This function splits a string into a dict.
    The string must be in the format: key0-value0#key1-value1#...#keyn-valuen
    """
    result = {}
    items = x.split("#")

    for i in items:
        key, value = i.split("-")
        try:
            result[key] = int(value)
        except:
            try:
                result[key] = float(value)
            except:
                result[key] = value

    return result


parser = argparse.ArgumentParser()
parser.add_argument("--jobs", default=1, type=int, help="The number of jobs to use for the evolution")
parser.add_argument("--seed", default=0, type=int, help="Random seed")
parser.add_argument("--n_actions", default=4, type=int, help="The number of action that the agent can perform in the environment")
parser.add_argument("--learning_rate", default="auto", help="The learning rate to be used for Q-learning. Default is: 'auto' (1/k)")
parser.add_argument("--df", default=0, type=float, help="The discount factor used for Q-learning")
parser.add_argument("--eps", default=0.05, type=float, help="Epsilon parameter for the epsilon greedy Q-learning")
parser.add_argument("--decay", default=1, type=float, help="Epsilon decay")
parser.add_argument("--input_space", default=12, type=int, help="Number of inputs given to the agent")
parser.add_argument("--lambda_", default=30, type=int, help="Population size")
parser.add_argument("--generations", default=1000, type=int, help="Number of generations")
parser.add_argument("--episodes", default=10, type=int, help="Number of episodes")
parser.add_argument("--cxp", default=0, type=float, help="Crossover probability")
parser.add_argument("--mp", default=1, type=float, help="Mutation probability")
parser.add_argument("--mutation", default="function-tools.mutUniformInt#low-0#up-40000#indpb-0.05", type=string_to_dict, help="Mutation operator. String in the format function-value#function_param_-value_1... The operators from the DEAP library can be used by setting the function to 'function-tools.<operator_name>'. Default: Uniform Int Mutation")
parser.add_argument("--crossover", default="function-tools.cxOnePoint", type=string_to_dict, help="Crossover operator, see Mutation operator. Default: One point")
parser.add_argument("--selection", default="function-tools.selTournament#tournsize-2", type=string_to_dict, help="Selection operator, see Mutation operator. Default: tournament of size 2")

parser.add_argument("--genotype_len", default=100, type=int, help="Length of the fixed-length genotype")
parser.add_argument("--low", default=0, type=float, help="Lower bound for the random initialization of the leaves")
parser.add_argument("--up", default=0, type=float, help="Upper bound for the random initialization of the leaves")
parser.add_argument("--types", default=None, type=str, help="This string must contain the range of constants for each variable in the format '#min_0,max_0,step_0,divisor_0;...;min_n,max_n,step_n,divisor_n'. All the numbers must be integers.")
parser.add_argument("--tol", default=0, type=int, help="Max disagreement allowed")


# Setup of the logging

date = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
logdir = "logs/gym/{}_{}".format(date, "".join(np.random.choice(list(string.ascii_lowercase), size=8)))
logfile = os.path.join(logdir, "log.txt")
os.makedirs(logdir)

args = parser.parse_args()

best = None
input_space_size = args.input_space
lr = "auto" if args.learning_rate == "auto" else float(args.learning_rate)


# Creation of an ad-hoc Leaf class

class CLeaf(RandomlyInitializedEpsGreedyLeaf):
    def __init__(self):
        """
        Initializes the leaf
        """
        RandomlyInitializedEpsGreedyLeaf.__init__(
            self,
            n_actions=args.n_actions,
            learning_rate=lr,
            discount_factor=args.df,
            epsilon=args.eps,
            low=0,
            up=0
        )
        self._decay = args.decay
        self._steps = 0

    def get_action(self):
        self.epsilon = self.epsilon * self._decay
        self._steps += 1
        action = super().get_action()
        return action


# Setup of the grammar
oblique_split = "+".join(["<const> * _in_{0}".format(i) for i in range(input_space_size)])

grammar = {
    "bt": ["import numpy as np\n<if>"],
    "if": ["if <condition>:{<action>}else:{<action>}"],
    # "condition": ["_in_{0} <= <const_type_{0}>".format(k) for k in range(input_space_size)],
    "condition": [
        "<var> <op> <const>",
        "<var> <op> <var>",
        # "<aggregator>(<var>, <var>) < <all>",
        # "(<var> <op> <const>) <logical_op> (<var> <op> <const>)"
    ] + \
    [
        # f"<aggregator>(_in_{i}, _in_{i+1}, _in_{i+2}, _in_{i+3}) <op> <int>" for i in [0, 4]
    ],
    "all": ["<const>", "<var>", "<int>"],
    "int": ["0", "1", "2", "3"],
    # "condition": [oblique_split + " < <const>".format(k) for k in range(input_space_size)],
    "aggregator": ["np.argmax", "np.max", "np.min", "np.argmin"],
    "var": ["_in_{}".format(i) for i in range(input_space_size)],
    "action": [f"out={i};leaf=\"_leaf\"" for i in range(4)] + \
              # [f"out=_in_{i};leaf=\"_leaf\"" for i in range(input_space_size)] + \
                ["<if>"] * 4,
    "const": [str(x) for x in np.arange(0, 1, 0.01)] + [f"{i}" for i in range(4)],
    "op": ["<", ">", "=="],
    "logical_op": ["and", "or"]
}

types = args.types if args.types is not None else ";".join(["0,1000,1,1000" for _ in range(input_space_size)])
types = types.replace("#", "")
assert len(types.split(";")) == input_space_size, "Expected {} types, got {}.".format(input_space_size, len(types.split(";")))

for index, type_ in enumerate(types.split(";")):
    rng = type_.split(",")
    start, stop, step, divisor = map(int, rng)
    consts_ = list(map(str, [float(c) / divisor for c in range(start, stop, step)]))
    grammar["const_type_{}".format(index)] = consts_

# print(grammar)


# Seeding of the random number generators

random.seed(args.seed)
np.random.seed(args.seed)


# Log all the parameters

with open(logfile, "a") as f:
    vars_ = locals().copy()
    for k, v in vars_.items():
        f.write("{}: {}\n".format(k, v))


# Definition of the fitness evaluation function

def evaluate_fitness(fitness_function, leaf, genotype, episodes=args.episodes):
    phenotype, _ = GrammaticalEvolutionTranslator(grammar).genotype_to_str(genotype)
    bt = PythonDT(phenotype, leaf)
    return fitness_function(bt, episodes)


def fitness(x, episodes=args.episodes):
    random.seed(args.seed)
    np.random.seed(args.seed)

    # Initialize the fitness
    fitness = [0, 0, 1]

    try:
        for i in range(4):
            global_cumulative_rewards = []
            # Load current fold
            X = pickle.load(open(f"Prognostic/X{i}.pkl", "rb"))
            y = pickle.load(open(f"Prognostic/y{i}.pkl", "rb"))

            # Define current metrics
            video_level_mse = 0
            exam_prediction = 0
            exam_level_mse = 0
            prognostic_acc = 0
            video_count = 0
            exam_count = 0

            for exam, gt in zip(X, y):
                for obs, gtvideo in zip(exam, gt):
                    x.new_episode()

                    # Compute the prediction
                    prediction = x(obs)

                    # Check that the tree is valid
                    if prediction is None:
                        raise ValueError()

                    # Update video-level stats
                    video_level_mse += (prediction - gtvideo) ** 2
                    video_count += 1

                    # Update exam-level stats
                    exam_prediction += prediction

                # Update the exam-level MSE
                exam_level_mse += (exam_prediction - sum(gt)) ** 2
                exam_count += 1

                # Update the prognostic stats
                if (exam_prediction > 24 and sum(gt) > 24) or \
                   (exam_prediction <= 24 and sum(gt) <= 24):
                    prognostic_acc += 1

            prognostic_acc = prognostic_acc / exam_count
            exam_level_mse = exam_level_mse / exam_count
            video_level_mse = video_level_mse / video_count

            # Use the opposite of MSE, since the algorithm used is for maximization
            if -video_level_mse < fitness[0]:
                fitness[0] = -video_level_mse
            if -exam_level_mse < fitness[1]:
                fitness[1] = -exam_level_mse
            if prognostic_acc < fitness[2]:
                fitness[2] = prognostic_acc

    except Exception as ex:
        # If the tree is not valid, assign worst fitness
        fitness = [-500, -500, 0]

    return fitness, x.leaves


def validate(phenotype):
    x = PythonDT(phenotype, CLeaf)
    X = pickle.load(open("Xv.pkl", "rb"))
    y = pickle.load(open("yv.pkl", "rb"))
    global_cumulative_rewards = []
    try:
        acc = 0
        count = 0
        cum_rew = 0
        for exam, gt in zip(X, y):
            score = 0
            for obs, gtvideo in zip(exam, gt):
                x.new_episode()
                count += 1

                prediction = x(obs)
                if prediction is None:
                    raise ValueError()
                if prediction == gtvideo:
                    acc += 1
                score += prediction

            # global_cumulative_rewards.append(- abs(score - sum(gt)))
            if (abs(score - sum(gt)) <= args.tol):
                global_cumulative_rewards.append(1)
            else:
                global_cumulative_rewards.append(0)

            # global_cumulative_rewards.append(- abs(score - sum(gt)))

    except Exception as ex:
        if len(global_cumulative_rewards) == 0:
            global_cumulative_rewards = [-50]

    return fitness


if __name__ == '__main__':
    import collections
    from joblib import parallel_backend

    def fit_fcn(x):
        return evaluate_fitness(fitness, CLeaf, x)

    with parallel_backend("multiprocessing"):
        pop = mo_grammatical_evolution(fit_fcn, inputs=input_space_size, leaf=CLeaf, individuals=args.lambda_, generations=args.generations, jobs=args.jobs, cx_prob=args.cxp, m_prob=args.mp, logfile=logfile, seed=args.seed, mutation=args.mutation, crossover=args.crossover, initial_len=args.genotype_len, selection=args.selection)


    # Log best individual

    with open(logfile, "a") as log_:
        for ind in pop:
            phenotype, _ = GrammaticalEvolutionTranslator(grammar).genotype_to_str(ind)
            phenotype = phenotype.replace('leaf="_leaf"', '')

            log_.write("Best\n")
            log_.write(phenotype + "\n")
            log_.write("best_fitness: {}".format(ind.fitness.values))
    with open(os.path.join(logdir, "fitness.tsv"), "w") as f:
        f.write(str(log))
